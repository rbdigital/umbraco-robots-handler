﻿using System.Configuration;
using System.IO;
using System.Web;
using RB.RobotsHandler.Constants;
using RB.RobotsHandler.Exceptions;
using RB.RobotsHandler.Helpers;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace RB.RobotsHandler.Handlers
{
    public class RobotsHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var url = context.Request.RawUrl;
            var path = context.Server.MapPath(url);

            // Do we have a file that already exists on the file system?
            // If so, always serve that.

            if (File.Exists(path))
            {
                LogHelper.Debug<RobotsHandler>("Streaming specified robots file from disk.");

                FileHelper.StreamFile(context, path);
                return;
            }

            UmbracoContextHelper.EnsureContext(context, ApplicationContext.Current);

            if (UmbracoContext.Current == null)
            {
                LogHelper.Debug<RobotsHandler>("Umbraco context is null even after ensuring we have a context.");
                throw new PageNotFoundException();
            }

            // Lets make sure we have all the config values we need.

            if (ConfigurationManager.AppSettings[AppSettingKeys.RobotsHandlerDocTypeAlias] == null ||
                ConfigurationManager.AppSettings[AppSettingKeys.RobotsHandlerPropertyAlias] == null)
            {
                LogHelper.Debug<RobotsHandler>("Missing app settings.");
                throw new PageNotFoundException();
            }

            var docType = ConfigurationManager.AppSettings[AppSettingKeys.RobotsHandlerDocTypeAlias];
            var propertyAlias = ConfigurationManager.AppSettings[AppSettingKeys.RobotsHandlerPropertyAlias];

            if (string.IsNullOrWhiteSpace(docType) || string.IsNullOrWhiteSpace(propertyAlias))
            {
                LogHelper.Debug<RobotsHandler>("App settings specified but missing values.");
                throw new PageNotFoundException();
            }

            // We have all config values we need so lets try and
            // find the robots file contents from Umbraco.

            var contents = UmbracoContentHelper.GetRobotsContent(context.Request.Url.Host, UmbracoContext.Current, docType, propertyAlias);

            if (string.IsNullOrWhiteSpace(contents))
            {
                LogHelper.Debug<RobotsHandler>("Doctype and property found but no contents have been specified.");
                throw new PageNotFoundException();
            }

            context.Response.Clear();
            context.Response.ContentType = "text/plain";
            context.Response.Write(contents);
            context.Response.End();
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
﻿namespace RB.RobotsHandler.Constants
{
    internal static class AppSettingKeys
    {
        public const string RobotsHandlerDocTypeAlias = "robotsHandlerDocTypeAlias";
        public const string RobotsHandlerPropertyAlias = "robotsHandlerPropertyAlias";
    }
}
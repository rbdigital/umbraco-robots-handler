﻿using System.Linq;
using umbraco.cms.businesslogic.web;
using Umbraco.Web;

namespace RB.RobotsHandler.Helpers
{
    internal static class UmbracoContentHelper
    {
        public static string GetRobotsContent(string host, UmbracoContext current, string docType, string propertyAlias)
        {
            var helper = new UmbracoHelper(current);
            var domain = Domain.GetDomain(host);

            // Find either the current root node for the domain
            // or use the first root node in the content tree
            var home = domain != null 
                ? helper.TypedContent(domain.RootNodeId) 
                : helper.TypedContentAtRoot().FirstOrDefault();

            if (home == null)
                return string.Empty;

            // Find the first instance of the doc type from
            // the current url domain
            var firstInsance = home.DescendantsOrSelf(docType).FirstOrDefault();

            // Must have a value
            if (firstInsance == null || !firstInsance.HasValue(propertyAlias))
                return string.Empty;

            return firstInsance.GetPropertyValue<string>(propertyAlias);
        }
    }
}
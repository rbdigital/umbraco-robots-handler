﻿using System.IO;
using System.Web;

namespace RB.RobotsHandler.Helpers
{
    internal static class FileHelper
    {
        public static void StreamFile(HttpContext context, string path)
        {
            var filename = Path.GetFileName(path);

            if (filename == null)
                return;

            var mimeType = MimeMapping.GetMimeMapping(filename);

            context.Response.ContentType = mimeType;
            context.Response.TransmitFile(path);
        }
    }
}
﻿using System.Web;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Security;

namespace RB.RobotsHandler.Helpers
{
    internal static class UmbracoContextHelper
    {
        public static void EnsureContext(HttpContext context, ApplicationContext current)
        {
            var contextWrapper = new HttpContextWrapper(context);

            UmbracoContext.EnsureContext(
                contextWrapper,
                ApplicationContext.Current,
                new WebSecurity(contextWrapper, ApplicationContext.Current),
                true);
        }
    }
}
﻿using System.Web;

namespace RB.RobotsHandler.Exceptions
{
    internal class PageNotFoundException : HttpException
    {
        public PageNotFoundException()
            : base(404, "Page Not Found")
        {
            
        }
    }
}